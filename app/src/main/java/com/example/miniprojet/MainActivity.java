package com.example.miniprojet;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.miniprojet.ResigterFragment.SignInFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import static com.example.miniprojet.Register.onresetpass;

public class MainActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPageable viewPagerdapter;

    FrameLayout framemain;

    TextView username, email;
    String _email, _username;

    DrawerLayout mdrawer;
    ActionBarDrawerToggle mtoggle;
private long backPressedTime;
    @Override
    public void onBackPressed() {
        if(backPressedTime+2000>System.currentTimeMillis())
        {
            super.onBackPressed();
            return;
        }else
        {
            Toast.makeText(getBaseContext(),"Press back again to exit ",Toast.LENGTH_LONG).show();
        }
backPressedTime=System.currentTimeMillis();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      //  viewPager=findViewById(R.id.view_pager_id);
        // framemain=findViewById(R.id.frame);


        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

//I added this if statement to keep the selected fragment when rotating the device
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.mainlayout,
                    new ProduitFragment()).commit();
        }

        mdrawer=findViewById(R.id.drawer);

        mtoggle=new ActionBarDrawerToggle(this,mdrawer,R.string.open,R.string.close);

         mdrawer.addDrawerListener(mtoggle);
        mtoggle.syncState();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);




        //viewPagerdapter=new ViewPageable(getSupportFragmentManager());
        // Add Fragment
       // viewPagerdapter.AddFragment(new ProduitFragment(),"");
        //viewPagerdapter.AddFragment(new GalleryFragment(),"");


        //viewpager bech taatih adapter
        //viewPager.setAdapter(viewPagerdapter);


//tablayout bech twa7alha maa viewpager
        //tabLayout.setupWithViewPager(viewPager);

        //tabLayout.getTabAt(0).setText("Robe");
        //  tabLayout.getTabAt(1).setText("Jupe");

       ActionBar actionBar=getSupportActionBar();
      actionBar.setElevation(0);
        NavigationView navigationView = findViewById(R.id.nav);
        updateHeader();

        setfragment(new ProduitFragment());

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();


                if (id == R.id.nav_my_produit) {
                    //setfragment(new Mes_ProduitFragment());

                   Intent intent = new Intent(getApplicationContext(), MesProduitActivity.class);
                    startActivity(intent);

                } else if (id == R.id.nav_Wishlist) {
                    Intent intent = new Intent(MainActivity.this, WishlistActivity.class);
                    startActivity(intent);

                } else if (id == R.id.nav_out) {

                    // fassa5 les donnes f sharedpref pour effacer la session
                    Intent intent = new Intent(getApplicationContext(), Register.class);
                    startActivity(intent);
finish();
                }else if(id==R.id.nav_model){
                    Intent intent = new Intent(getApplicationContext(), ModelActivity.class);
                    startActivity(intent);
                }else if(id==R.id.add_produit){
                    setfragment( new AjoutFragment());


                }

                return false;
            }
        });
        navigationView.getMenu().getItem(0).setCheckable(true);




    }





    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(mtoggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateHeader(){
        NavigationView navigationView = findViewById(R.id.nav);
        View header=navigationView.getHeaderView(0);

        username = header.findViewById(R.id.main_fullname);
        email = header.findViewById(R.id.main_email);
        _email = getIntent().getStringExtra("email");
        _username = getIntent().getStringExtra("username");
        System.out.println(_email);
        System.out.println(_username);

        email.setText(_email.toString());
        username.setText(_username.toString());

    }


    public void setfragment(Fragment fragment){
        FragmentTransaction fragmentTransaction= this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainlayout,fragment);
        fragmentTransaction.commit();
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                    switch (item.getItemId()) {
                        case R.id.home:
                            setfragment( new ProduitFragment());
                            break;
                        case R.id.add_produit:
                            setfragment( new AjoutFragment());


                            break;
                        case R.id.compte:
                            setfragment( new ProduitFragment());
                            break;
                    }




                    return true;
                }
            };
}




