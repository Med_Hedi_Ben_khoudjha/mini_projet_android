package com.example.miniprojet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.daimajia.swipe.implments.SwipeItemMangerImpl;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.ResigterFragment.SignInFragment;
import com.example.miniprojet.entity.Produit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Mes_ProduitAdapter extends RecyclerSwipeAdapter<Mes_ProduitAdapter.MyViewHolder> {
    private Context context;
    private List<Produit> produitList;
    private RequestOptions option;

    RequestQueue c;


    Mes_ProduitAdapter(Context context, List<Produit> produitList) {
        this.context = context;
        this.produitList = produitList;
        option=new RequestOptions().centerCrop().placeholder(R.drawable.pantalon).error(R.drawable.robe);}

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.mes_produit_item, parent, false);
         MyViewHolder holder = new MyViewHolder(view);



        return holder;
    }






    @SuppressLint("Assert")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Produit produit = produitList.get(position);



        holder.stock.setText("Stock : "+ String.valueOf(produit.getStock()));
        holder.size.setText("Size : "+String.valueOf(produit.getSize()));
        holder.pix.setText("Prix :"+String.valueOf(produit.getPrix()));


        Glide.with(context).load(produit.getUrl()).apply(option)
                .into(holder.imf_product);

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        //dari kiri
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, holder.swipeLayout.findViewById(R.id.bottom_wrapper1));

        //dari kanan
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wraper));



        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        holder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        holder.btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Clicked on Information " + holder.pix.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        holder.Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemManger.removeShownLayouts(holder.swipeLayout);
                produitList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, produitList.size());
                mItemManger.closeAllItems();
                jsonrequest(produit.getUrl());

            }
        });


        mItemManger.bindView(holder.itemView, position);


        System.out.println(produit.getUrl());
        System.out.println(produit.getPrix());
        System.out.println(produit.getDescription());


    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public int getItemCount() {
        return produitList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView pix,stock,size,modif;
        public ImageView imf_product,delete;

        public SwipeLayout swipeLayout;
        public TextView Delete;
        public TextView Edit;
        public TextView Share;
        public ImageButton btnLocation;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imf_product =itemView.findViewById(R.id.img_produit_);
            pix=itemView.findViewById(R.id._prix_);
            stock=itemView.findViewById(R.id.stock_);
            size=itemView.findViewById(R.id.size_);
           // modif=itemView.findViewById(R.id.modif_);

            delete=itemView.findViewById(R.id.acheter);


            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            Delete = (TextView) itemView.findViewById(R.id.Delete);


            btnLocation = (ImageButton) itemView.findViewById(R.id.btnLocation);

        }


    }


    private void jsonrequest(final String url){

        StringRequest dr = new StringRequest(Request.Method.POST, AppConfig.URL_delete_Produit,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Toast.makeText(context, "Produit Supprimer", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error.

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<String, String>();


                  params.put("url",url);

                return params;
            }
        };
        c = Volley.newRequestQueue(context);

        c.add(dr);
    }



}
