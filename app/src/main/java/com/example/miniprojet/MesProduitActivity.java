package com.example.miniprojet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.swipe.util.Attributes;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.ResigterFragment.SignInFragment;
import com.example.miniprojet.entity.Produit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MesProduitActivity extends AppCompatActivity {




     List<Produit> produitList;

     RequestQueue c;


     RecyclerView recycler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes_produit);
        recycler = findViewById(R.id.racy);

        c = Volley.newRequestQueue(this);
System.out.println(String.valueOf(SignInFragment.id)+"id");
        produitList=new ArrayList<>();

        jsonrequest();

        //imageView = root.findViewById(R.id.image);





    }


   public   void jsonrequest(){

        JsonObjectRequest requests=new JsonObjectRequest(Request.Method.POST, AppConfig.URL_List_Produit,null,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("JSONResponse",response.toString());



                try {


                    JSONArray jsonArray =response.getJSONArray("produit");
                    if(jsonArray.length()<=0){
                        Toast.makeText(getApplicationContext(),"Rien",Toast.LENGTH_LONG).show();
                    }else{
                        for (int i=0;i<jsonArray.length();i++) {

                            JSONObject jsonObject=jsonArray.optJSONObject(i);
//                        String trouver = jsonObject.getString("trouver");


                            String description = jsonObject.optString("description");
                            String type = jsonObject.optString("type");
                            String url = jsonObject.optString("url");
                            String prix = jsonObject.optString("prix");
                            String id_user = jsonObject.optString("id_user");
                            String name = jsonObject.optString("name");
                            String quantite = jsonObject.optString("quantite");
                            String stock = jsonObject.optString("stock");
                            String size = jsonObject.optString("size");

                            System.out.println(prix);


                           System.out.println(description);
                           System.out.println(type);
                            System.out.println(url);
                            System.out.println(id_user);
                            System.out.println(quantite);
                            System.out.println(size);
                            System.out.println(stock);
if(id_user.equals(String.valueOf(SignInFragment.id))){

                            produitList.add(new Produit(type,
                                    description,
                                    prix,
                                    Integer.parseInt(id_user),
                                    url,
                                    name,quantite,stock,size));}

                        }}



                }
                catch (JSONException e) {
                    e.printStackTrace();

                }



                superview(produitList);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("lina "+error.getMessage());
                Log.d("EreurRequest","Ereur"+ error.getMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<String, String>();


              //  params.put("id_user",String.valueOf(SignInFragment.id));

                return params;
            }
        };

        c.add(requests);
    }
    long animationduration =1000;
    private void superview(List<Produit> productList) {

        LinearLayoutManager firstManager = new LinearLayoutManager(this);

        //layoutManager = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(firstManager);

        recycler.setHasFixedSize(true);
        Mes_ProduitAdapter apter=new Mes_ProduitAdapter(this,productList);




        ((Mes_ProduitAdapter) apter).setMode(Attributes.Mode.Single);

        recycler.setAdapter(apter);

        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("RecyclerView", "onScrollStateChanged");
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        //recycler.setAdapter(apter);
        // adapter.notifyDataSetChanged();
    }



}
