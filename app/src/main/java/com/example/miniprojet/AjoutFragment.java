package com.example.miniprojet;


import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.ResigterFragment.SignInFragment;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * A simple {@link Fragment} subclass.
 */
public class AjoutFragment extends Fragment {
    TextView number_Txt;
    Spinner spinner;
    FrameLayout framemain;
    Button add, moins_B, plus_B;
    EditText description, prix;
    ImageView img_produit;
    private int counter;
    RequestQueue c;
    List<Integer> drapeau;
    List<String> pays;
    private Uri filepath;

    private Bitmap bitmap;
    private static final int STORAGE_PERMISSION_CODE = 4655;
    private int PICK_IMAGE_REQUEST = 1;


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.plus_n:
                    pluscounter();
                    break;
                case R.id.moins_n:
                    moinscounter();
                    break;
            }
        }
    };

    public AjoutFragment() {
        // Required empty public constructor
    }

    private final String CHANNEL_ID="Personnel Notifications";
    private final int NOTIFICATION_ID=001;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ajout, container, false);
        spinner = view.findViewById(R.id.spnner);
        add = view.findViewById(R.id.btn_add);

        moins_B = view.findViewById(R.id.moins_n);
        moins_B.setOnClickListener(onClickListener);

        plus_B = view.findViewById(R.id.plus_n);
        plus_B.setOnClickListener(onClickListener);

        number_Txt = view.findViewById(R.id.number_);


        description = view.findViewById(R.id.txt_description);

        img_produit = view.findViewById(R.id.ImageView);
        prix = view.findViewById(R.id.edit_prix);
        img_produit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(v);
            }
        });
        initcounter();

        requestStoragePermission();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData(v);

                // recreate();
                String Message="Produit ajouter";
                NotificationCompat.Builder builder=new NotificationCompat.Builder(getActivity(),CHANNEL_ID);
                builder.setSmallIcon(R.drawable.logo);
                builder.setContentTitle("New Notification");
                builder.setContentText(Message);
                builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
                NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(getActivity());
                notificationManagerCompat.notify(NOTIFICATION_ID,builder.build());
            }
        });

        drapeau = new ArrayList<>();
        pays = new ArrayList<>();


        pays.add("Robe");
        pays.add("Talon");

        drapeau.add(R.drawable.pantalon);
        drapeau.add(R.drawable.robe);
        drapeau.add(R.drawable.jupe);
        drapeau.add(R.drawable.talon);
        drapeau.add(R.drawable.pull);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, pays);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinner.setAdapter(dataAdapter);


        return view;
    }

    private void initcounter() {
        counter = 0;
        System.out.println(counter);
        moins_B.setEnabled(false);
        number_Txt.setText(counter + "");
    }

    private void pluscounter() {

        counter++;
        System.out.println(counter);
        moins_B.setEnabled(true);

        number_Txt.setText(counter + "");

    }

    private void moinscounter() {

        if (counter == 0) {
            moins_B.setEnabled(false);

        } else {
            counter--;
            number_Txt.setText(counter + "");


        }

    }

    void requestStoragePermission() {

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getContext(), "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    void ShowFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {

            filepath = data.getData();
            try {

                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), filepath);
                img_produit.setImageBitmap(bitmap);
            } catch (Exception ex) {

            }
        }
    }

    void selectImage(View view) {
        ShowFileChooser();
    }




    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContext().getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    void uploadImage() {
        final String im_description = description.getText().toString();
        final String im_type = spinner.getSelectedItem().toString();
        final String im_prix = prix.getText().toString();
        final String im_quantite = number_Txt.getText().toString();
        final String im_stock = "En Stock";
        final String currentDateTimeString = DateFormat.getDateInstance().format(new Date());

        String path = getPath(filepath);
        System.out.println("path: "+path);

        try {
            String uploadId = UUID.randomUUID().toString();

            //new MultipartUploadRequest(this, uploadId, AppConfig.URL_INSERT_PRODUIT)
            //.addParameter("name",currentDateTimeString)
            //.addFileToUpload(path, "image")
            //.addParameter("id_user", String.valueOf(SignInFragment.id))
            //.addParameter("description",im_description)
            //.addParameter( "type",im_type)
            //.addParameter("prix",im_prix)
            //.addParameter("quantite",im_quantite)
            //.addParameter("size",im_quantite)
            //.addParameter("stock",im_stock)

            // .setNotificationConfig(new UploadNotificationConfig())
            //.setMaxRetries(3)
            //.startUpload();


            StringRequest dr = new StringRequest(Request.Method.POST, AppConfig.URL_image,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            // response
                            Toast.makeText(getContext(), "Ajouter ", Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error.

                        }
                    }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params=new HashMap<String, String>();

                    String path = getPath(filepath);

                    params.put("name", currentDateTimeString);
                    params.put("prix",im_prix);
                    params.put("description",im_description);
                    params.put("type",im_type);
                    params.put("stock",im_stock);
                    params.put("id_user",String.valueOf(SignInFragment.id));

                    params.put("size",im_quantite);
                    params.put("quantite",im_quantite);
                    params.put("image",imageToString(bitmap));

                    return params;
                }
            };
            c = Volley.newRequestQueue(getContext());

            c.add(dr);

        } catch (Exception ex) {
            Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();
            System.out.println();
            Toast.makeText(getContext(),ex.getMessage(),Toast.LENGTH_LONG).show();


        }


    }








    public void saveData(View view)
    {
        uploadImage();

    }

    private String imageToString (Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[]imagebyte= byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imagebyte,Base64.DEFAULT);
    }

}

