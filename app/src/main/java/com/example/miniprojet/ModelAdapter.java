package com.example.miniprojet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.entity.Produit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ModelAdapter extends RecyclerView.Adapter<ModelAdapter.MyViewHolder> {
    private Context context;
    private List<Produit> produitList;
    private RequestOptions option;

    RequestQueue c;


    ModelAdapter(Context context, List<Produit> produitList) {
        this.context = context;
        this.produitList = produitList;
        option=new RequestOptions().centerCrop().placeholder(R.drawable.pantalon).error(R.drawable.robe);}

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.mes_model_item, parent, false);

        MyViewHolder holder = new MyViewHolder(view);




        return holder;
    }



public  Produit produitk=null;


    @SuppressLint("Assert")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Produit produit = produitList.get(position);

        //url=produitList.get(position).getUrl();
        holder.stock.setText("Stock : "+ String.valueOf(produit.getStock()));
        holder.size.setText("Size : "+String.valueOf(produit.getSize()));
        holder.pix.setText("Prix :"+String.valueOf(produit.getPrix()));

;
        Glide.with(context).load(produit.getUrl()).apply(option)
                .into(holder.imf_product);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 produitk=produitList.get(position);
                System.out.println(produitk.getUrl());

                Intent intent=new Intent(context, ModelActivity.class);
                intent.putExtra("url",produitk.getUrl());
               context.startActivity(intent);

            }
        });


    }



    @Override
    public int getItemCount() {
        return produitList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

         TextView pix,stock,size,modif;
         ImageView imf_product,delete;

         RelativeLayout swipeLayout;
         TextView Delete;
         TextView Edit;
         TextView Share;

        public ImageButton btnLocation;

        @SuppressLint("CutPasteId")
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imf_product =itemView.findViewById(R.id.img_produit__);
            pix=itemView.findViewById(R.id._prix__);
            stock=itemView.findViewById(R.id.stock__);
            size=itemView.findViewById(R.id.size__);
           // modif=itemView.findViewById(R.id.modif_);




            swipeLayout = (RelativeLayout) itemView.findViewById(R.id.swipeo);

          //  parentLayout = itemView.findViewById(R.id.swipe);




        }


    }


    private void jsonrequest(final String url){

        StringRequest dr = new StringRequest(Request.Method.POST, AppConfig.URL_delete_fav,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Toast.makeText(context, "Supprimer", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error.

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<String, String>();


                  params.put("url",url);

                return params;
            }
        };
        c = Volley.newRequestQueue(context);

        c.add(dr);
    }




}
