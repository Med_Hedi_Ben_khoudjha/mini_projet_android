package com.example.miniprojet.entity;

public class Produit {

    private int id;
    private String type;
    private String description;
    private String prix;
    private String quantite;
    private String stock;
    private int id_user;
    private String url;
    String size;
    private String name;

    public String getStock() {
        return stock;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public String isStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Produit(String type, String description, String prix, int id_user, String url, String name,
                   String quantite, String stock, String size) {
        this.type = type;
        this.description = description;
        this.prix = prix;
        this.id_user = id_user;
        this.url = url;
        this.name = name;
        this.quantite=quantite;
        this.stock=stock;
        this.size=size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
