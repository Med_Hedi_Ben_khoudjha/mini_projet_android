package com.example.miniprojet.ResigterFragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.MainActivity;
import com.example.miniprojet.ModelActivity;
import com.example.miniprojet.R;
import com.example.miniprojet.Register;
import com.example.miniprojet.entity.Produit;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener{


    private android.content.SharedPreferences mPreferences;
    public static int id;
    public static final String sharedPrefFile = "tn.esprit.ECom";
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private FrameLayout ParentFramLayout;

    private ProgressBar progressBar;
    RequestQueue c;


private static final int REQUEST_CODE=9001;
GoogleApiClient googleApiClient;
SignInButton sign;
    TextInputEditText _emailText;
    TextInputEditText _passwordText;
    Button _loginButton;
    TextView _signupLink;
    TextView link_forgot;
    List<Produit> produitList;


    private String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";

    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        ParentFramLayout = getActivity().findViewById(R.id.Register_frameLayout);
        _signupLink = view.findViewById(R.id.link_signup);
        //sign=view.findViewById(R.id.sign_in_button);
        _loginButton = view.findViewById(R.id.btn_login);
        _passwordText=view.findViewById(R.id.input_password);
        _emailText=view.findViewById(R.id.input_email);
       // firebaseAuth = FirebaseAuth.getInstance();
        progressBar = view.findViewById(R.id.progressBar);
        link_forgot=view.findViewById(R.id.link_forgot);
        //getSharedPreferences sauvegarde login et password
        mPreferences =  getContext().getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        c = Volley.newRequestQueue(getContext());
//sign.setOnClickListener(this);

          _emailText.setText( mPreferences.getString(("email"),"") );

        _passwordText.setText( mPreferences.getString(("password"), "") );
        return view;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_CODE){
            GoogleSignInResult result =Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleresult(result);
            //Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
           // handleSignInResult(task);
        }

    }

    private void handleresult(GoogleSignInResult result) {
        if(result.isSuccess()){
            GoogleSignInAccount account=result.getSignInAccount();
            String username=account.getDisplayName();
            String email=account.getEmail();
            String profil_url=account.getPhotoUrl().toString();
            updateUi(true);
        }else {
            updateUi(false);

        }
    }

    private void updateUi(boolean b) {
if(b){
    //profile_section.setVisibility(View.VISIBLE);
    sign.setVisibility(View.GONE);
    //Intent intent=new Intent(getContext(),MainActivity.class);
   // startActivity(intent);

}else {
    sign.setVisibility(View.GONE);

}
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        progressBar.setVisibility(View.INVISIBLE);

        link_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register.onresetpass=true;
                setfragment(new ForgotPasswordFragment());

            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register.onresetpass=true;

                setfragment(new SignUpFragment());
            }


        });
_emailText.addTextChangedListener(new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //verifier si le mail et valide
        checkinput();

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
});
_passwordText.addTextChangedListener(new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
checkinput();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
});


_loginButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        Verifier_Email_et_Pass();
        progressBar.setVisibility(View.INVISIBLE);

    }
});
    }

    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right, R.anim.slideout_from_left);
        fragmentTransaction.replace(R.id.Register_frameLayout, fragment);
        fragmentTransaction.commit();
    }

    private void checkinput() {
        if (!TextUtils.isEmpty(_emailText.getText())) {
            if (!TextUtils.isEmpty(_passwordText.getText())) {
                _loginButton.setEnabled(true);


                }
             else {

            }

        } else {
            _loginButton.setEnabled(false);
        }

    }
    private void Verifier_Email_et_Pass() {
        if (!isEmail(_emailText)) {
            _emailText.setError("Enter valid email!");
        }
       else if (isEmpty(_passwordText)) {
            _passwordText.setError("Champs Vide!");
            //seterror yekteblek bel rouge "champs vide"
        }
        else{




            StringRequest request=new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        String trouver =jsonObject.getString("trouver");
                        if(trouver.equals("1")){
                            String username =jsonObject.getString("username");
                            String email =jsonObject.getString("email");
                             id =jsonObject.getInt("id");

                            progressBar.setVisibility(View.VISIBLE);
                            SharedPreferences.Editor editor=mPreferences.edit();
                            editor.putString("email",_emailText.getText().toString());
                            editor.putString("password",_passwordText.getText().toString());
                            editor.apply();

                            Intent intent=new Intent(getActivity(), MainActivity.class);

                            // c'est pas la peine de faire putextra ( envoyer les données avec intent ) 5ater déja msajel maa SharedPreference les données
                            intent.putExtra("email",email.toString());
                            intent.putExtra("username",username.toString());
                            intent.putExtra("id",id);
                            System.out.println(username);
                            System.out.println(email);
                            startActivity(intent);

                        }else if(trouver.equals("0")){
                            _emailText.setText("");
                            _passwordText.setText("");
                            _emailText.setError("Vérifier vos données!");


                        }

                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println(error.getMessage());
                    Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();

                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params=new HashMap<String, String>();


                    params.put("email",_emailText.getText().toString());
                    params.put("password",_passwordText.getText().toString());
                    return params;
                }
            };
            c.add(request);

        }


    }

    private void jsonrequest(){

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, AppConfig.URL_PRIX,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("JSONResponse",response.toString());


                try {


                    JSONArray jsonArray =response.getJSONArray("produit");
                    if(jsonArray.length()==0){
                        Toast.makeText(getContext(),"Rien",Toast.LENGTH_LONG).show();
                    }else{
                        for (int i=0;i<jsonArray.length();i++) {

                            JSONObject jsonObject=jsonArray.getJSONObject(i);
//                        String trouver = jsonObject.getString("trouver");


                            String description = jsonObject.getString("description");
                            String type = jsonObject.getString("type");
                            String url = jsonObject.getString("url");
                            String prix = jsonObject.getString("prix");
                            String id_user = jsonObject.getString("id_user");
                            String name = jsonObject.getString("name");
                            String size = jsonObject.getString("size");
                            String stock = jsonObject.getString("stock");
                            String quantite = jsonObject.getString("quantite");
//                        System.out.println(prix);
//                        System.out.println(description);
//                        System.out.println(type);
//                        System.out.println(url);
//                        System.out.println(id_user);


                            produitList.add(new Produit(type,
                                    description,
                                    prix,
                                    Integer.parseInt(id_user),
                                    url,
                                    name,quantite,stock,size ));

                        }}
                    final String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
                    Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(currentDateTimeString);

                    for (Produit produit:produitList
                         ) {


                    }


                }
                catch (JSONException e) {
                    e.printStackTrace();

                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                Log.d("EreurRequest","Ereur"+ error.getMessage());

            }
        });
        c = Volley.newRequestQueue(getContext());

        c.add(request);
    }


    private boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    private boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        //Patterns.EMAIL_ADDRESS verifier si le mail valide ou non la forme juste la forma et si il est vide
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}