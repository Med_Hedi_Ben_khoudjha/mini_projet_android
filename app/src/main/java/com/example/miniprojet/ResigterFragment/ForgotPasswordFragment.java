package com.example.miniprojet.ResigterFragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.TransitionManager;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.R;
import com.example.miniprojet.entity.SendMail;
import com.google.android.material.textfield.TextInputEditText;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends Fragment {
    private TextInputEditText email;
    private AppCompatButton envoyer;
    RequestQueue c;
    private TextView msg;
    private ImageView img_email;
    private ProgressBar progressBar;


    private ViewGroup emailcontainer;
    private TextView back;
    private FrameLayout ParentFramLayout;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        email = view.findViewById(R.id.in_email);
        envoyer = view.findViewById(R.id.btn_envoyer);
        msg = view.findViewById(R.id.msg);
        ParentFramLayout = getActivity().findViewById(R.id.Register_frameLayout);
        back = view.findViewById(R.id.back);
        img_email = view.findViewById(R.id.img_email);

        c = Volley.newRequestQueue(getContext());


        emailcontainer = view.findViewById(R.id.forgot);
        progressBar = view.findViewById(R.id.progressBar2);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setfragment(new SignInFragment());


            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkinput();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        envoyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TransitionManager.beginDelayedTransition(emailcontainer);
                img_email.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);

                //envoyer.setEnabled(false);


//                firebaseAuth.sendPasswordResetEmail(email.getText().toString())
//                        .addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                if (task.isSuccessful()) {
//                                    ScaleAnimation animation = new ScaleAnimation(1, 0, 1, 0);
//                                    animation.setDuration(100);
//                                    animation.setInterpolator(new AccelerateInterpolator());
//                                    animation.setRepeatMode(Animation.REVERSE);
//                                    animation.setRepeatCount(1);
//                                    animation.setAnimationListener(new Animation.AnimationListener() {
//                                        @Override
//                                        public void onAnimationStart(Animation animation) {
//                                            img_email.setVisibility(View.VISIBLE);
//
//                                        }
//
//                                        @Override
//                                        public void onAnimationEnd(Animation animation) {
//
//                                        }
//
//                                        @Override
//                                        public void onAnimationRepeat(Animation animation) {
//
//                                        }
//                                    });
//
//                                    Toast.makeText(getActivity(),"Envoyer !",Toast.LENGTH_LONG).show();
//
//
//                                    img_email.startAnimation(animation);
//                                    msg.setVisibility(View.VISIBLE);
//
//
//                                } else {
//                                    String error = task.getException().getMessage();
//                                    envoyer.setEnabled(true);
//
//                                    msg.setText(error);
//                                    msg.setTextColor(getResources().getColor(R.color.Red));
//
//                                    TransitionManager.beginDelayedTransition(emailcontainer);
//                                    msg.setVisibility(View.VISIBLE);
//
//                                }
//                                progressBar.setVisibility(View.GONE);
//
//                            }
//                        });
//
//
                jsonrequest();
           }
        });
    }

    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right, R.anim.slideout_from_left);
        fragmentTransaction.replace(ParentFramLayout.getId(), fragment);
        fragmentTransaction.commit();
    }



    private void jsonrequest(){
        StringRequest request=new StringRequest(Request.Method.POST, AppConfig.URL_passRecup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    String trouver =jsonObject.getString("trouver");
                    if(trouver.equals("1")){

                        String password =jsonObject.getString("password");

                        String mail = email.getText().toString();
                        // String subject = editTextSubject.getText().toString().trim();
                        //String message = editTextMessage.getText().toString().trim();

                        //Creating SendMail object
                        SendMail sm = new SendMail(getContext(), mail, "Recuperer Mot Passe ", password);

                        //Executing sendmail to send email
                        sm.execute();
                        //progressBar.setVisibility(View.VISIBLE);

                        ScaleAnimation animation = new ScaleAnimation(1, 0, 1, 0);
                                    animation.setDuration(100);
                                    animation.setInterpolator(new AccelerateInterpolator());
                                    animation.setRepeatMode(Animation.REVERSE);
                                    animation.setRepeatCount(1);
                                    animation.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                            img_email.setVisibility(View.VISIBLE);

                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                        }

                                        @Override
                                       public void onAnimationRepeat(Animation animation) {
                                        }
                                    });

                                    Toast.makeText(getActivity(),"Envoyer !",Toast.LENGTH_LONG).show();

                        img_email.startAnimation(animation);
                                   msg.setVisibility(View.VISIBLE);





                    }else if(trouver.equals("0")){


                                  envoyer.setEnabled(true);

                                    msg.setText("Verifier Votre email");
                                    msg.setTextColor(getResources().getColor(R.color.Red));

                                    TransitionManager.beginDelayedTransition(emailcontainer);
                                    msg.setVisibility(View.VISIBLE);
Toast.makeText(getContext(),"Verifier Votre email",Toast.LENGTH_LONG).show();
                    }
                                             progressBar.setVisibility(View.GONE);


                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<String, String>();


                params.put("email",email.getText().toString());
                //params.put("password",_passwordText.getText().toString());
                return params;
            }
        };
        c.add(request);

    }

    private void checkinput() {
        if (!TextUtils.isEmpty(email.getText())) {
            envoyer.setEnabled(true);


        } else {
            envoyer.setEnabled(false);
        }

    }

}
