package com.example.miniprojet.ResigterFragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.R;
import com.example.miniprojet.Register;
import com.google.android.material.textfield.TextInputEditText;



import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */


/**
 * Firestore est une base de données flexible et évolutive
 * pour le développement mobile, Web
 * et serveur de Firebase et de Google Cloud Platform.
 * Comme la base de données en temps réel Firebase,
 * elle synchronise vos données entre les applications clientes et les écouteurs en temps réel.
 * Elle offre également une prise en charge hors ligne pour mobile et Web,
 * de sorte que vous puissiez créer des applications
 * réactives fonctionnant indépendamment de la latence du réseau
 * ou de la connectivité Internet.
 * Cloud Firestore offre également une intégration transparente
 * avec d'autres produits Firebase et Google Cloud Platform,
 * y compris les fonctions cloud.
 */
public class SignUpFragment extends Fragment {
    private FrameLayout ParentFramLayout;

    private TextView link_login;
    RequestQueue c;
    private EditText email;
    private EditText fullname;
    private TextInputEditText password;
    private TextInputEditText confirmpassw;
    private Button signup;
    private ProgressBar progressBar;


    private String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";
  static String url="http://192.168.1.136/miniprojet/register.php";
    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        link_login = view.findViewById(R.id.link_login);

        email = view.findViewById(R.id.input_email);
        fullname = view.findViewById(R.id.input_name);
        password = view.findViewById(R.id.input_password);
        confirmpassw = view.findViewById(R.id.Confirm_password);
        signup = view.findViewById(R.id.btn_signup);
        progressBar = view.findViewById(R.id.progressBar);

        c = Volley.newRequestQueue(getContext());

        ParentFramLayout = getActivity().findViewById(R.id.Register_frameLayout);
        //firestore=FirebaseFirestore.getInstance();

        return view;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        link_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register.onresetpass=true;

                setfragment(new SignInFragment());

            }
        });



        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//badel esm w nathem 5edmtek bech baad mathi3ech bis
                Verifier_Email_et_Pass();
            }
        });
    }

    private void Verifier_Email_et_Pass() {
        if (isEmpty(fullname)) {
            fullname.setError("Nom vide!");
            //a9al men 10 caracteres fil layout conditions
        }

      else  if (!isEmail(email)) {
            email.setError("Enter valid email!");
        }
       else if (isEmpty(password)) {
            password.setError("Champs Vide!");
        }
        else if (isEmpty(confirmpassw)) {
            confirmpassw.setError("Champs Vide!");
        }
        else if(!(password.getText().toString().equals(confirmpassw.getText().toString()))) {
            confirmpassw.setError("Mot passe non identique!");
        }
        else{




        StringRequest  request=new StringRequest(Request.Method.POST, AppConfig.URL_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    String ajouter =jsonObject.getString("ajouter");
                    if(ajouter.equals("1")){
                        progressBar.setVisibility(View.VISIBLE);
//
//                        Intent intent=new Intent(getContext(),MainActivity.class);
//                        intent.putExtra("email",email.toString());
//                        intent.putExtra("username",fullname.toString());
//                       // intent.putExtra("id",id);
//                        startActivity(intent);
//                        getActivity().finish();
                        setfragment(new SignInFragment());
                    }else if(ajouter.equals("-1")){
                        email.setError("Cet'utilisateur existe déjà!");

                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<String, String>();

                params.put("username",fullname.getText().toString());
                params.put("email",email.getText().toString());
                params.put("password",password.getText().toString());
                return params;
            }
        };
        c.add(request);

}
    }

    private void checkinput() {
        if (!TextUtils.isEmpty(email.getText())) {
            if (!TextUtils.isEmpty(fullname.getText())) {

                if (!TextUtils.isEmpty(password.getText()) && password.length() < 8) {

                    if (!TextUtils.isEmpty(confirmpassw.getText())) {
                        signup.setEnabled(true);
                    } else {

                    }
                } else {

                }
            } else {

            }

        } else {
            signup.setEnabled(false);
        }

    }

    private boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    private boolean isEmail(EditText text) {
        CharSequence email = text.getText().toString();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }
    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_from_left, R.anim.slideout_from_right);

        fragmentTransaction.replace(R.id.Register_frameLayout, fragment);
        fragmentTransaction.commit();
    }

}
