package com.example.miniprojet;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.entity.Produit;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public  class ProduitFragment extends Fragment {



    List<Produit> produitList;

    RequestQueue c;
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    // JsonArrayRequest  c;
    ViewPager viewPager;

    private robeAdapter adapter;





    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("WrongConstant")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        //jsonRequeqt();

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //  viewPager=root.findViewById(R.id.viewpager);

        produitList=new ArrayList<>();

        jsonrequest();

        recyclerView = root.findViewById(R.id.recy_categ);
        //imageView = root.findViewById(R.id.image);





        EditText editText = root.findViewById(R.id.edittext);
        InputMethodManager imm =  (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText,InputMethodManager.HIDE_IMPLICIT_ONLY);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });


        for (Produit produit: produitList) {
            System.out.println("prix " + produit.getPrix());
            System.out.println("type "+ produit.getType());
            System.out.println("url " +produit.getUrl());
            System.out.println(" description " +produit.getDescription());
            System.out.println("id_user "+produit.getId_user());
        }









        return root;
    }

    private void filter(String text) {
        ArrayList<Produit> filteredList = new ArrayList<>();

        for (Produit item : produitList) {
            if (item.getType().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }

        adapter.filterList(filteredList);
    }
    private void jsonrequest(){

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, AppConfig.URL_List_Produit,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("JSONResponse",response.toString());


                try {


                    JSONArray jsonArray =response.getJSONArray("produit");
                    if(jsonArray.length()==0){
                        Toast.makeText(getContext(),"Rien",Toast.LENGTH_LONG).show();
                    }else{
                    for (int i=0;i<jsonArray.length();i++) {

                        JSONObject jsonObject=jsonArray.getJSONObject(i);
//                        String trouver = jsonObject.getString("trouver");


                        String description = jsonObject.getString("description");
                        String type = jsonObject.getString("type");
                        String url = jsonObject.getString("url");
                        String prix = jsonObject.getString("prix");
                        String id_user = jsonObject.getString("id_user");
                        String name = jsonObject.getString("name");
                        String size = jsonObject.getString("size");
                        String stock = jsonObject.getString("stock");
                        String quantite = jsonObject.getString("quantite");
//                        System.out.println(prix);
//                        System.out.println(description);
//                        System.out.println(type);
//                        System.out.println(url);
//                        System.out.println(id_user);


                        produitList.add(new Produit(type,
                                description,
                                prix,
                                Integer.parseInt(id_user),
                                url,
                                name,quantite,stock,size ));

                    }}



                }
                catch (JSONException e) {
                    e.printStackTrace();

                }



                setuprecycleview(produitList);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                Log.d("EreurRequest","Ereur"+ error.getMessage());

            }
        });
        c = Volley.newRequestQueue(getContext());

        c.add(request);
    }
long animationduration =1000;
    private void setuprecycleview(List<Produit> produitList) {

        LinearLayoutManager firstManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        //layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(firstManager);

        recyclerView.setHasFixedSize(true);
         adapter=new robeAdapter(getContext(),produitList);

        recyclerView.setAdapter(adapter);
        // adapter.notifyDataSetChanged();
    }




}
