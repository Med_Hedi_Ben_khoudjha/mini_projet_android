package com.example.miniprojet;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.paypal.android.sdk.payments.PaymentActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentDetails extends AppCompatActivity {

    TextView txtid,txtaccount,txtstatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        txtid=findViewById(R.id.txtid);
        txtaccount=findViewById(R.id.txtaccount);
        txtstatus=findViewById(R.id.txtstatus);

        Intent intent=getIntent();
        try {
            JSONObject jsonObject=new JSONObject(intent.getStringExtra("PaymentDetails"));
            showdetails(jsonObject.getJSONObject("response"),intent.getStringExtra("PaymentAccount"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void showdetails(JSONObject response, String paymentAccount) {
        try {
            txtid.setText(response.getString("id"));
            txtaccount.setText(response.getString("state"));
            txtstatus.setText("$"+paymentAccount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
