package com.example.miniprojet;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.miniprojet.Config.AppConfig;
import com.example.miniprojet.ResigterFragment.SignInFragment;
import com.example.miniprojet.entity.Produit;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class robeAdapter extends RecyclerView.Adapter<robeAdapter.MyViewHolder> implements Filterable {
    private Context context;
    private List<Produit> produitList;
    private Dialog dialog;
    private RequestOptions option;
    private long DURATION = 500;
    private boolean on_attach = true;
    private List<Produit> ProduitListAll;
    RequestQueue c;


    public List<Produit> getProduitList() {
        return produitList;
    }

    public void setProduitList(List<Produit> produitList) {
        this.produitList = produitList;
    }

    robeAdapter(Context context, List<Produit> produitList) {
        this.context = context;
        this.produitList = produitList;
        this.ProduitListAll=new ArrayList<>(produitList);
        option=new RequestOptions().centerCrop().placeholder(R.drawable.pantalon).error(R.drawable.robe);}

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.produit_item, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);
        dialog = new Dialog(context,R.style.Theme_AppCompat_Light_Dialog);
        dialog.setContentView(R.layout.dialog_content);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Dialog dialog2 = new Dialog(context);


       holder.details.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View v) {
                TextView dialog_type = dialog.findViewById(R.id.dialog_type);
                ImageView dialog_acheter = dialog.findViewById(R.id.dialog_btn_acheter);
                TextView dialog_prix = dialog.findViewById(R.id.dialog_prix);
                TextView dialog_descrip = dialog.findViewById(R.id.dialog_descrip);
                ImageView dialog_img = dialog.findViewById(R.id.dialog_img);
                dialog_acheter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(context,PayPalActivity.class);
                        intent.putExtra("prix",produitList.get(holder.getAdapterPosition()).getPrix());
                        context.startActivity(intent);
                    }
                });
                dialog_type.setText(produitList.get(holder.getAdapterPosition()).getType());
                dialog_prix.setText("Prix :" +produitList.get(holder.getAdapterPosition()).getPrix());
                dialog_descrip.setText(produitList.get(holder.getAdapterPosition()).getDescription());


                Glide.with(context).load(produitList.get(holder.getAdapterPosition()).getUrl()).apply(option)
                        .into(dialog_img);
                Toast.makeText(context, "Test" + String.valueOf(holder.getAdapterPosition()), Toast.LENGTH_LONG);
                dialog.show();
            }
        });





        return holder;
    }



    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Produit produit = produitList.get(position);
        //setAnimation(holder.itemView, position);
        //FromLeftToRight(holder.itemView, position);
holder.fav.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.Theme_AppCompat_Light_DarkActionBar);
        builder.setCancelable(true);
        builder.setTitle("Wishlist ");

        builder.setIcon(R.drawable.ic_favorite_red_24dp);
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        holder.fav.setImageResource(R.drawable.ic_favorite_red_24dp);
                        jsonrequest(produit.getUrl(),
                                produit.getPrix(),
                                produit.getStock(),
                                produit.getSize(),
                                produit.getDescription(),
                                produit.getType());
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();



    }
});
        holder.stock.setText(produit.getStock());
        holder.size.setText("Size : "+produit.getSize());
        holder.prix.setText("Prix : "+produit.getPrix());
        holder.acheter.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent=new Intent(context,PayPalActivity.class);
        intent.putExtra("prix",produitList.get(holder.getAdapterPosition()).getPrix());

        context.startActivity(intent);

    }
});

        Glide.with(context).load(produit.getUrl()).apply(option)
                .into(holder.img_produit); // biblio bch taffichi taswira te5ou url w taffichi



        System.out.println(produit.getUrl());
        System.out.println(produit.getPrix());
        System.out.println(produit.getDescription());


    }



    @Override
    public int getItemCount() {
        return produitList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView prix,stock,size,details,solder;

        ImageView img_produit,acheter,fav;


        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img_produit=itemView.findViewById(R.id.img_produit);
            details=itemView.findViewById(R.id.details);
            prix=itemView.findViewById(R.id._prix);
            stock=itemView.findViewById(R.id.stock);
            size=itemView.findViewById(R.id.size);

            acheter=itemView.findViewById(R.id.acheter);
            fav=itemView.findViewById(R.id.fav);

        }


    }

    public void filterList(ArrayList<Produit> filteredList) {
        produitList = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }

    private Filter myFilter = new Filter() {

        //Automatic on background thread
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            List<Produit> filteredList = new ArrayList<>();

            if (charSequence == null || charSequence.length() == 0) {
                filteredList.addAll(ProduitListAll);
            } else {
                for (Produit produit : ProduitListAll) {
                    if (produit.getType().contains(charSequence.toString().toLowerCase())) {
                        filteredList.add(produit);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        //Automatic on UI thread
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            produitList.clear();
            produitList.addAll((Collection<? extends Produit>) filterResults.values);
            notifyDataSetChanged();
        }

    };




        private void setAnimation(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", 0.f, 0.5f, 1.0f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animator.setStartDelay(isNotFirstItem ? DURATION / 2 : (i * DURATION / 3));
        animator.setDuration(500);
        animatorSet.play(animator);
        animator.start();
    }

    private void FromLeftToRight(View itemView, int i) {
        if(!on_attach){
            i = -1;
        }
        boolean not_first_item = i == -1;
        i = i + 1;
        itemView.setTranslationX(-400f);
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(itemView, "translationX", -400f, 0);
        ObjectAnimator animatorAlpha = ObjectAnimator.ofFloat(itemView, "alpha", 1.f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animatorTranslateY.setStartDelay(not_first_item ? DURATION : (i * DURATION));
        animatorTranslateY.setDuration((not_first_item ? 2 : 1) * DURATION);
        animatorSet.playTogether(animatorTranslateY, animatorAlpha);
        animatorSet.start();
    }

    private void jsonrequest(final String url

    ,final String prix
    ,final String stock
    ,final String size
    ,final String description
            ,final String type
    ){

        StringRequest dr = new StringRequest(Request.Method.POST, AppConfig.URL_fav,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                       // Toast.makeText(context, "Produit Supprimer", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error.

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<String, String>();


                params.put("url",url);
                params.put("id_user",String.valueOf(SignInFragment.id));
                params.put("prix",prix);
                params.put("stock",stock);
                params.put("size",size);
                params.put("description",description);
                params.put("type",type);

                return params;
            }
        };
        c = Volley.newRequestQueue(context);

        c.add(dr);
    }



}
