package com.example.miniprojet.Config;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AppConfig {


    // Server user login url
    public static String URL_LOGIN = "https://miniprojet2019.000webhostapp.com/login.php";


    // Server user login url
    public static String URL_PRIX = "https://miniprojet2019.000webhostapp.com/login.php";

    // Server user register url
    public static String URL_REGISTER = "https://miniprojet2019.000webhostapp.com/register.php";

    // Server list produit url
    public static String URL_List_Produit = "https://miniprojet2019.000webhostapp.com/List_Produit.php";

    // Server insert produit url
    public static String URL_INSERT_PRODUIT= "https://miniprojet2019.000webhostapp.com/Insert_Produit.php";

    // Server list mes  produit url
    public static String URL_MES_PRODUIT= "https://miniprojet2019.000webhostapp.com/Mes_produit.php";

    // Server insert produit url
    public static String URL_passRecup= "https://miniprojet2019.000webhostapp.com/passRecup.php";

    // Server insert produit url
    public static String URL_delete_Produit= "https://miniprojet2019.000webhostapp.com/delete_Produit.php";

    // Server insert produit url
    public static String URL_update_Produit= "https://miniprojet2019.000webhostapp.com/upload.php";

    // Server insert produit url
    public static String URL_fav= "https://miniprojet2019.000webhostapp.com/fav.php";

    public static String URL_Mes_fav= "https://miniprojet2019.000webhostapp.com/mes_fav.php";
    public static String URL_delete_fav= "https://miniprojet2019.000webhostapp.com/delete_fav.php";
    public static String URL_image= "https://miniprojet2019.000webhostapp.com/image.php";


}
