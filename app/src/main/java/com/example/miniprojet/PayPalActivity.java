package com.example.miniprojet;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.miniprojet.Config.config;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

public class PayPalActivity extends AppCompatActivity {

private static final int PAYPAL_REQUEST_CODE=7171;
private static PayPalConfiguration configuration=new PayPalConfiguration()
        .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(config.PayPal_Client_ID);
Button paypal;
EditText editaccount;
String account="";

    @Override
    protected void onDestroy() {
      stopService(new Intent(this,PayPalService.class));
        super.onDestroy();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_pal);
        paypal=findViewById(R.id.pay_now);
        editaccount=findViewById(R.id.editaccount);
        String prix=getIntent().getStringExtra("prix");
        editaccount.setText(prix);


        //star paypal service
        Intent intent=new Intent(this,PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,configuration);
        startService(intent);

        paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processPaypal();
            }
        });


    }

    private void processPaypal() {

    account=editaccount.getText().toString();
        PayPalPayment payPalPayment=new PayPalPayment(new BigDecimal(String.valueOf(account)),"USD","Payement",PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent=new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,configuration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payPalPayment);
        startActivityForResult(intent,PAYPAL_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PAYPAL_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                assert data != null;
                PaymentConfirmation confirmation=data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if(confirmation!=null){
                    try {
                        String paymentdetails=confirmation.toJSONObject().toString(4);
                        startActivity(new Intent(this,PaymentDetails.class)
                                .putExtra("PaymentDetails",paymentdetails)
                        .putExtra("PaymentAccount",account));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }else if(resultCode== Activity.RESULT_CANCELED) {
                Toast.makeText(this,"Close",Toast.LENGTH_LONG).show();
            }
        }else if(resultCode==PaymentActivity.RESULT_EXTRAS_INVALID){
            Toast.makeText(this,"Invalid",Toast.LENGTH_LONG).show();


        }

        super.onActivityResult(requestCode, resultCode, data);

    }
}
